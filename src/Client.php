<?php
namespace amoCrm;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use amoCrm\Entity\AmoEntityBase;

class Client {

  protected $httpClient;

  protected $authResponse = NULL;

  protected $domain;

  protected $login;

  protected $apiKey;

  protected $requestType = 'json';

  protected $lastException;

  public function __construct($domain, $login, $api_key) {

    $config = [
      'headers' => [
        'User-Agent' => 'amoCrmAPIClient',
      ],
      'base_uri' => 'https://' . $domain,
      'cookies' => true,
    ];



    $this->httpClient = new GuzzleHttpClient($config);

    $this->domain = $domain;
    $this->login = $login;
    $this->apiKey = $api_key;

    $this->auth();

    AmoEntityBase::setApiClient($this);

  }

  public function auth() {

    if (!$this->domain || !$this->login || !$this->apiKey) {
      throw new \Exception('Required client params not defined');
    }

    $options = [
      RequestOptions::JSON => [
        'USER_LOGIN' => $this->login,
        'USER_HASH' => $this->apiKey,
      ],
      'query' => [
        'type' => 'json'
      ],
    ];

    try {
      $response = $this->httpClient->post('/private/api/auth.php', $options);

      $json_decode = new JsonDecode();

      $this->authResponse = $json_decode->decode($response->getBody()->getContents(), JsonEncoder::FORMAT)->response;

      return $this->authResponse->auth;
    }
    catch (\Exception $e) {
      $this->lastException = $e;
      return FALSE;
    }

  }

  public function isAuth() {
    return $this->authResponse ? $this->authResponse->auth : FALSE;
  }

  public function request($method, $uri, array $json_data_array = [], array $options = []) {

    $request_options = [];

    if ($json_data_array) {
      $request_options[RequestOptions::JSON] = $json_data_array;
    }

    $request_options = array_merge($request_options, $options);
    $response = $this->httpClient->request($method, $uri, $request_options);

    $result = json_decode($response->getBody()->getContents());

    return $result;

  }


  protected function getResponseResult() {

  }

}

