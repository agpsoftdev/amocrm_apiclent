<?php
namespace amoCrm\Entity;

use amoCrm\Client;

abstract class AmoEntityBase {

  protected $id;

  protected static $apiEndpoint = '';

  /**
   * @var Client
   */
  protected static $apiClient;

  public function id() {
    return $this->id();
  }

  public function isNew() {
    return $this->id ? FALSE : TRUE;
  }

  public function save() {

  }

  public static function load($id) {

  }


  public static function loadMultiple($ids = []) {

  }

  public static function list($parametrs = []) {

    if (!static::$apiClient) {
      throw new \Exception('AmoCRM API client not initialized');
    }

    $options = [];

    if ($parametrs) {
      $options['query'] = $parametrs;
    }

    $result = self::$apiClient->request('GET', static::$apiEndpoint, [], $options);

    if (!empty($result->_embedded->items)) {
      return $result->_embedded->items;
    }

  }

  public static function setApiClient(Client $client) {
    static::$apiClient = $client;
  }

  public static function create() {
    return new static();
  }


}

